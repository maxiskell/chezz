<?php

require_once 'board.php';
require_once 'pieces.php';

/**
 * Return the best next move for a given knight, based on its safetiness.
 *
 * @param  array $board      The chess board.
 * @param  array $knight     The knight current position.
 * @param  array $queen      The queen current position.
 * @return array
 */
function nextKnightMove(array $board, array $knight, array $queen)
{
    $points = array_filter(
        knightMoves($board, $knight),
        function ($m) use ($queen) {
            return safeForKnight($m, $queen);
        }
    );

    // if not safe, stay where you are!
    if (count($points) == 0) {
        return $knight;
    }

    return nearest($points, $queen);
}

/**
 * Return the best next move for a given queen, based on its distance to a
 * knight.
 *
 * @param array $board      The chess board.
 * @param array $queen      The queen current position.
 * @param array $knights    The knights positions.
 * @retun array
 */
function nextQueenMove(array $board, array $queen, array $knights)
{
    $nearestKnight = nearest($knights, $queen);

    return validQueenMove($nearestKnight, $queen) ?
        $nearestKnight : nearestToKnight($board, $nearestKnight, $queen);
}

/**
 * Simulate a game turn for a given piece and return the action string.
 *
 * @param string $who       The piece name.
 * @param array  $board     The chess board.
 * @param array  $pieces    The remaining pieces.
 * @retun string
 */
function playTurn(string $who, array &$board, array &$pieces)
{
    $from = $pieces[$who];

    switch ($who) {
        case 'k1':
            $next = nextKnightMove($board, $pieces['k1'], $pieces['q']);
            $piece = KNIGHT;
            $pieces['k1'] = $next;
            if ($next == $pieces['q']) {
                unset($pieces['q']);
            }
            break;
        case 'k2':
            $next = nextKnightMove($board, $pieces['k2'], $pieces['q']);
            $piece = KNIGHT;
            $pieces['k2'] = $next;
            if ($next == $pieces['q']) {
                unset($pieces['q']);
            }
            break;
        case 'q':
            $knights = [$pieces['k1'] ?? [-1,0], $pieces['k2'] ?? [-1,0]];
            $next = nextQueenMove($board, $pieces['q'], $knights);
            $piece = QUEEN;
            $pieces['q'] = $next;
            if ($next == $knights[0]) {
                unset($pieces['k1']);
            } else if ($next == $knights[1]) {
                unset($pieces['k2']);
            }
            break;
    }

    allocatePiece(NIL, $board, $from);
    allocatePiece($piece, $board, $next);

    return playLog($who, $from, $next);
}

/**
 * Check the state of the game given the current remaining pieces.
 *
 * @param  array $pieces  The current remaining pieces.
 * @return bool
 */
function checkGame(array $pieces)
{
    if ((!isset($pieces['k1']) && !isset($pieces['k2'])) ||
        (!isset($pieces['q']))) {
        return false;
    }

    return true;
}

/**
 * Shows the winner message.
 */
function playLog(string $who, array $from, array $to)
{
    $log  = PLAYERS[$who].' from ';
    $log .= coordinatesToPosition($from);
    $log .= ' to '.coordinatesToPosition($to).PHP_EOL;

    return $log;
}

/**
 * Main game loop.
 */
function chezz(string $k1, string $k2, string $q)
{
    $board  = generateBoard();
    $run = true;
    $pieces = [
        'k1' => positionToCoordinates($k1),
        'q' => positionToCoordinates($q),
        'k2' => positionToCoordinates($k2)
    ];

    $current = '';

    allocatePiece(KNIGHT, $board, $pieces['k1']);
    allocatePiece(KNIGHT, $board, $pieces['k2']);
    allocatePiece(QUEEN, $board, $pieces['q']);

    printBoard($board);

    $log = fopen('chezz.log', 'w');

    while ($run) {
        foreach ($pieces as $k => &$p) {
            $current = $k;
            $turn = playTurn($k, $board, $pieces);
            echo $turn;
            fwrite($log, $turn);

            $run = checkGame($pieces);
            printBoard($board);
        }
    }

    $winStr = (strpos($current, 'k') === false ? 'Black' : 'White').' wins!';
    echo $winStr;

    fwrite($log, $winStr);
    fclose($log);
}
