<?php

require_once 'board.php';

define('QUEEN', '♛');

/**
 * Return the nearest valid queen movement to a given knight position.
 *
 * @param  array $board     The chess board.
 * @param  array $knight    The knight position.
 * @param  array $queen     The queen position.
 *
 * @return array;
 */
function nearestToKnight(array &$board, array $knight, array $queen)
{
    $ki = $knight[0];
    $kj = $knight[1];
    $point = [];

    $surroundings = [
        [$ki-1,$kj],
        [$ki,$kj+1],
        [$ki,$kj-1],
        [$ki,$kj+1],
        [$ki-1,$kj-1],
        [$ki-1,$kj+1],
        [$ki+1,$kj-1],
        [$ki+1,$kj+1]
    ];

    foreach ($surroundings as $s) {
        if (isset($board[$s[0]][$s[1]]) && validQueenMove($s, $queen))  {
            $point = $s;
            break;
        }
    }

    return $point;
}

/**
 * Return whether a movement (destination point) is valid from the current
 * queen position.
 *
 * @param array $point  The point to evaluate.
 * @param array $queen  The current queen position.
 */
function validQueenMove(array $point, array $queen)
{
    return (($point[0] === $queen[0] || $point[1] === $queen[1]) || 
      (abs($queen[0] - $point[0]) === abs($queen[1] - $point[1])));
}
