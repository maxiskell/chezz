<?php

define('BLACK', '[■]');
define('WHITE', '[ ]');
define('NIL', '0');

define('ALPHA_TO_INDEX', [
    'a' => 0,
    'b' => 1,
    'c' => 2,
    'd' => 3,
    'e' => 4,
    'f' => 5,
    'g' => 6,
    'h' => 7,
]);
define('INDEX_TO_ALPHA', [
    0 => 'a',
    1 => 'b',
    2 => 'c', 
    3 => 'd',
    4 => 'e',
    5 => 'f',
    6 => 'g',
    7 => 'h'
]);

/**
 * Return an empty chess board.
 *
 * @return array
 */
function generateBoard() : array
{
    $board = [];

    foreach (range(0, 7) as $i) {
        foreach (range(0, 7) as $j) {
            $board[$i][$j] = '0';
        }
    }

    return $board;
}

/**
 * Allocate a piece in a board.
 *
 * @param  string $piece        The representation of the piece being allocated.
 * @param  array  $board        The chess board.
 * @param  array  $position     The position where to allocate the piece in.
 * @return void
 */
function allocatePiece(string $piece, array &$board, array $position)
{
    $i = $position[0];
    $j = $position[1];

    if (isset($board[$i][$j])) {
        $board[$i][$j] = $piece;
    }
}

/**
 * Print a rudimentary representation of the board.
 *
 * @param  array $board  The chess board to be printed.
 * @return void
 */
function printBoard(array $board)
{
    echo '   A  B  C  D  E  F  G  H '.PHP_EOL;

    foreach (range(0, 7) as $i) {
        echo ($i+1).' ';
        foreach (range(0, 7) as $j) {
            if ($board[$i][$j] === '0') {
                echo ($i % 2 === 0) ?
                    (($j % 2 === 0) ? WHITE : BLACK) :
                    (($j % 2 === 0) ? BLACK : WHITE);
            } else {
                echo '['.$board[$i][$j].']';
            }
        }

        echo PHP_EOL;
    }

    echo PHP_EOL;
}

/**
 * Return the Tchebychev distance between two given points.
 *
 * @param  array $p
 * @param  array $q
 * @return int
 */
function distance(array $p, array $q)
{
    return max(
        abs($p[0] - $q[0]),
        abs($p[1] - $q[1])
    );
}

/**
 * Return the nearest point to another from a given array of points.
 *
 * @param array $points     The points to be evaluated.
 * @param array $to         The main point.
 */
function nearest(array $points, array $to)
{
    $dist = 100; // arbitrary high distance

    if (count($points) == 0) {
        return $points[0];
    }

    foreach ($points as $p) {
        if ($p[0] >= 0) {
            $newDist = distance($p, $to);

            if ($newDist < $dist) {
                $nearest = $p;
                $dist = $newDist;
            }
        }
    }

    return $nearest;
}

/**
 * Return an array of corrdinates from a string representation.
 *
 * @param  string $position   Position expresed as string (e.g. 'a1')
 * @return array
 */
function positionToCoordinates(string $position) : array
{
    $split = str_split($position);
    $column = strtolower($split[0]);
    $row = (int) $split[1];

    if (!array_key_exists($column, ALPHA_TO_INDEX) || ($row > 8 || $row < 1)) {
        exit('ERROR: Invalid position '. $position);
    }

    return [$row-1, ALPHA_TO_INDEX[$column]];
}

/**
 * Return a position string from a given array of corrdinates.
 *
 * @param  string $coordinates Position expresed as string (e.g. 'a1')
 * @return array
 */
function coordinatesToPosition(array $coordinates) : string
{
    return strtoupper(INDEX_TO_ALPHA[$coordinates[1]]).($coordinates[0]+1);
}
