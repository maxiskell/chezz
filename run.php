<?php

require_once 'chezz.php';

if (count($argv) < 4) {
    $title = 'Chezz';
    die($title.PHP_EOL.'Usage: run.php <knight#1 position> <knight#2 position> <queen position>');
}

chezz($argv[1], $argv[2], $argv[3]);
