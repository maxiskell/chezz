# Chezz

## The challenge

Simulate a movement for one of the horses to the reachable position which is closest to the
queen avoiding the horse to be killed by the queen in its next move.
If the horse kills the queen, stop the program.
Simulate a movement for the queen to the reachable position which is closest to one of the
horses.
If the queen already killed the two horses after the move, stop the program.
If both players survive, repeat the steps above.
Once the program ends, inform how many moves were performed and what piece killed the
other.

## Run it

To run the simulation, pop a console, cd to the repo and simply type:

```
$ php run.php <knight#1 position> <knight#2 position> <queen position>
```
Example:

```
$ php run.php a1 b5 f3
```

Optionally, you could save the cool graphics to a file:

```
$ php run.php <knight#1 position> <knight#2 position> <queen position> > /tmp/coolchezz
```

## About the implementation

Although I am familiar, cofortable and have plenty of experience with OOP and design patterns in PHP (one can model pieces and boards polyphormistically), I took a functional/ modular approach to solve this computer-intensive task to avoid the extra work of resolving auto-loading, class instantiating, circular dependency and another OOP problems not related to the task itself that may unnecessarily bloat a small piece of software. Another advantage is it made the program easier to write/maintain (for me), easier to read and follow (for you) and way easier to compute (for machines).

## The solution

In each turn, depending on the piece, a couple of decisions have to be made to determine the destination of the named piece.

In the case of the knights, the algorithm is the following:

* Check the available moves from the current stand point
* discard those that will let the queen it the knight in the next move
* from the remaining, chose the nearest move to the queen.

The case of the queen works as follows:

* Check which knight is the nearest
* in case that knight is positioned at a valid queen's move, go there and eat it
* otherwise, move to a position nearest to the knight as possible (its surroundings).

## Visualization

In addition to the required logging, the program writes to the stardard output a rudimentary representation of the state of the board after the move.
