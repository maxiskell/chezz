<?php

define('KNIGHT', '♞');

/**
 * Return the available knight moves in a given board from a starting position.
 *
 * @param  array $board      The chess board.
 * @param  array $position   The current knight position.
 * @return array
 */
function knightMoves(array $board, array $position) : array
{
    $moves = [];
    $i = $position[0];
    $j = $position[1];

    if (isset($board[$i+1][$j-2])) {
        $moves[] = [$i+1, $j-2];
    }

    if (isset($board[$i+2][$j-1])) {
        $moves[] = [$i+2, $j-1];
    }

    if (isset($board[$i+2][$j+1])) {
        $moves[] = [$i+2, $j+1];
    }

    if (isset($board[$i+1][$j+2])) {
        $moves[] = [$i+1, $j+2];
    }

    if (isset($board[$i-1][$j+2])) {
        $moves[] = [$i-1, $j+2];
    }

    if (isset($board[$i-2][$j+1])) {
        $moves[] = [$i-2, $j+1];
    }

    if(isset($board[$i-2][$j-1])) {
        $moves[] = [$i-2, $j-1];
    }

    if (isset($board[$i-1][$j-2])) {
        $moves[] = [$i-1, $j-2];
    }

    return $moves;
}

/**
 * Return whether a given move is safe for a knight, which means the queen
 * can not eat it in the next move.
 *
 * @param array $point  The point to evaluate.
 * @param array $queen  The current queen position.
 */
function safeForKnight(array $point, array $queen)
{
    return ($point == $queen) ||
      (($point[0] !== $queen[0] && $point[1] !== $queen[1]) &&
      (abs($queen[0] - $point[0]) !== abs($queen[1] - $point[1])));
}
